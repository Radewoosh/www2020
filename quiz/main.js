function czas() {
    return (new Date()).getTime();
}
function touch(str) {
    var x = JSON.parse(window.localStorage.getItem(str));
    if (x == null)
        x = [];
    window.localStorage.setItem(str, JSON.stringify(x));
}
function tryb() {
    return window.localStorage.getItem('tryb');
}
function dodaj_wynik(nick, wynik) {
    touch(tryb());
    var x = JSON.parse(window.localStorage.getItem(tryb()));
    x.push({ wynik: wynik, nick: nick });
    window.localStorage.setItem(tryb(), JSON.stringify(x));
}
function aktualizuj_wyniki() {
    touch(tryb());
    var el = document.querySelector('.miejsce_na_wyniki');
    while (el.children.length > 1)
        el.removeChild(el.lastElementChild);
    var list = JSON.parse(window.localStorage.getItem(tryb()));
    list.sort(function (a, b) {
        if (a.wynik < b.wynik)
            return -1;
        if (a.wynik > b.wynik)
            return 1;
        return 0;
    });
    while (list.length > 10)
        list.pop();
    var id = 0;
    for (var i in list) {
        id++;
        var wstaw = document.createElement('tr');
        wstaw.innerHTML = '<td>' + id + '</td>' + '<td>' + list[i].nick + '</td>' + '<td>' + list[i].wynik + '</td>';
        el.appendChild(wstaw);
    }
}
function wylacz(str) {
    var el = document.querySelector(str);
    el.style.display = 'none';
}
function wlacz(str) {
    var el = document.querySelector(str);
    el.style.display = 'block';
}
function wylacz_wszystko() {
    wylacz('.menu');
    wylacz('.wyniki');
    wylacz('.quiz');
    wylacz('.koniec_quizu');
}
function idz_do_menu() {
    wylacz_wszystko();
    wlacz('.menu');
}
function idz_do_wyniki() {
    wylacz_wszystko();
    aktualizuj_wyniki();
    wlacz('.wyniki');
}
function idz_do_quiz() {
    wylacz_wszystko();
    wlacz('.quiz');
}
function reset_czasow() {
    var list = [];
    var dlu = daj_aktualny_quiz().pytania.length;
    for (var i = 1; i <= dlu; i++)
        list.push(0);
    window.localStorage.setItem('czasy_start', JSON.stringify(list));
    window.localStorage.setItem('czasy_koniec', JSON.stringify(list));
    window.localStorage.setItem('moje_odp', JSON.stringify(list));
}
function zacznij_quiz() {
    idz_do_quiz();
    ustal_quiz();
    reset_czasow();
    idz_do_pytania(1);
}
var quiz_easy = "{\n    \"pytania\":\n    [\n        \"7*8\",\n        \"46+85\",\n        \"39+87\",\n        \"2+2*2\"\n    ],\n    \"odpowiedzi\":\n    [\n        \"56\",\n        \"131\",\n        \"126\",\n        \"6\"\n    ],\n    \"kary\":\n    [\n        \"8\",\n        \"15\",\n        \"15\",\n        \"5\"\n    ]\n}";
var quiz_hard = "{\n    \"pytania\":\n    [\n        \"7821+1399\",\n        \"8433+9232\",\n        \"54*76\",\n        \"12*115+34\"\n    ],\n    \"odpowiedzi\":\n    [\n        \"9220\",\n        \"17665\",\n        \"4104\",\n        \"1414\"\n    ],\n    \"kary\":\n    [\n        \"20\",\n        \"20\",\n        \"45\",\n        \"60\"\n    ]\n}";
function random_int(min, max) {
    return Math.floor(min + Math.random() * (max - min + 1));
}
function quiz_rand() {
    var pyt = '';
    var odp = '';
    var kar = '';
    var dlu = 10;
    for (var i = 0; i < dlu; i++) {
        if (i > 0) {
            pyt += ',';
            odp += ',';
            kar += ',';
        }
        if (random_int(0, 1) == 0) {
            var a = random_int(0, 100);
            var b = random_int(0, 100);
            pyt += '"' + a.toString() + '*' + b.toString() + '"';
            odp += '"' + (a * b).toString() + '"';
            kar += '"45"';
        }
        else {
            var a = random_int(0, 10000);
            var b = random_int(0, 10000);
            pyt += '"' + a.toString() + '+' + b.toString() + '"';
            odp += '"' + (a + b).toString() + '"';
            kar += '"20"';
        }
    }
    return '{"pytania":[' + pyt + '],"odpowiedzi":[' + odp + '],"kary":[' + kar + ']}';
}
function wybierz(str) {
    var el1 = document.querySelector('.wybor_easy');
    var el2 = document.querySelector('.wybor_hard');
    var el3 = document.querySelector('.wybor_rand');
    el1.style.backgroundColor = '';
    el2.style.backgroundColor = '';
    el3.style.backgroundColor = '';
    el1.style.border = '';
    el2.style.border = '';
    el3.style.border = '';
    var el = document.querySelector(str);
    el.style.backgroundColor = 'rgb(10, 126, 243)';
    el.style.border = '3px solid rgb(51, 153, 255)';
}
function wybierz_easy() {
    wybierz('.wybor_easy');
    idz_do_menu();
    window.localStorage.setItem('tryb', 'easy');
}
function wybierz_hard() {
    wybierz('.wybor_hard');
    idz_do_menu();
    window.localStorage.setItem('tryb', 'hard');
}
function wybierz_rand() {
    wybierz('.wybor_rand');
    idz_do_menu();
    window.localStorage.setItem('tryb', 'rand');
}
wybierz_easy();
function ustal_quiz() {
    window.localStorage.setItem('quiz', daj_jakis_quiz());
}
function daj_jakis_quiz() {
    if (tryb() == "easy")
        return quiz_easy;
    if (tryb() == "hard")
        return quiz_hard;
    if (tryb() == "rand")
        return quiz_rand();
}
function daj_aktualny_quiz() {
    return JSON.parse(window.localStorage.getItem('quiz'));
}
function daj_aktualne_pytanie() {
    return parseInt(window.localStorage.getItem('numer_pytania'));
}
function idz_do_pytania(x) {
    czysc_odp();
    var el = document.querySelector(".quiz header");
    el.textContent = "PYTANIE " + (x.toString());
    var quiz = daj_aktualny_quiz();
    el = document.querySelector(".info_o_pytaniu");
    el.textContent = quiz.pytania[x - 1] + "=?   ( " + quiz.kary[x - 1] + "s kary )";
    window.localStorage.setItem('numer_pytania', x.toString());
    el = document.querySelector('.skoki_do_pytan');
    while (el.children.length > 0)
        el.removeChild(el.lastElementChild);
    var dlu = daj_aktualny_quiz().pytania.length;
    var wez2 = JSON.parse(window.localStorage.getItem('czasy_koniec'));
    var _loop_1 = function (i) {
        if ((i == 0 && x == 1) || (i == dlu + 1 && x == dlu))
            return "continue";
        var wstaw = document.createElement('button');
        var kt = i;
        if (i == 0)
            kt = x - 1;
        if (i == dlu + 1)
            kt = x + 1;
        wstaw.className = "wybor klikalne";
        wstaw.onclick = function () { idz_do_pytania(kt); };
        wstaw.innerText = i.toString();
        if (i == 0)
            wstaw.innerText = "POPRZEDNIE";
        if (i == dlu + 1)
            wstaw.innerText = "NASTEPNE";
        if (wez2[kt - 1] > 0) {
            wstaw.style.backgroundColor = 'rgb(10, 126, 243)';
            wstaw.style.border = '3px solid rgb(51, 153, 255)';
        }
        el.appendChild(wstaw);
    };
    for (var i = 0; i <= dlu + 1; i++) {
        _loop_1(i);
    }
    if (wszystko_mam()) {
        var wstaw = document.createElement('button');
        wstaw.className = "wybor klikalne";
        wstaw.onclick = function () { zakoncz_quiz(); };
        wstaw.innerText = "ZAKOŃCZ QUIZ";
        el.appendChild(wstaw);
    }
    var wez1 = JSON.parse(window.localStorage.getItem('czasy_start'));
    if (wez1[x - 1] == 0)
        wez1[x - 1] = czas();
    window.localStorage.setItem('czasy_start', JSON.stringify(wez1));
    var wez3 = JSON.parse(window.localStorage.getItem('moje_odp'));
    if (wez2[x - 1] == 0) {
        el = document.querySelector(".miejsce_na_zapisana");
        el.textContent = "";
    }
    else {
        el = document.querySelector(".miejsce_na_zapisana");
        el.textContent = "Twoja odpowiedź to: " + wez3[x - 1].toString();
    }
}
function wszystko_mam() {
    var wez = JSON.parse(window.localStorage.getItem('czasy_koniec'));
    var dlu = wez.length;
    for (var i = 0; i < dlu; i++)
        if (wez[i] == 0)
            return false;
    return true;
}
function czysc_odp() {
    var el = document.getElementById("odpowiedz");
    el.value = "";
}
function zatwierdz_odpowiedz() {
    var el = document.getElementById("odpowiedz");
    if (el.value == "")
        return;
    var wez1 = JSON.parse(window.localStorage.getItem('czasy_koniec'));
    wez1[daj_aktualne_pytanie() - 1] = czas();
    window.localStorage.setItem('czasy_koniec', JSON.stringify(wez1));
    var wez2 = JSON.parse(window.localStorage.getItem('moje_odp'));
    wez2[daj_aktualne_pytanie() - 1] = parseInt(el.value);
    window.localStorage.setItem('moje_odp', JSON.stringify(wez2));
    czysc_odp();
    idz_do_pytania(daj_aktualne_pytanie());
}
function czysc_nick() {
    var el = document.getElementById("nick");
    el.value = "";
}
function zakoncz_quiz() {
    czysc_nick();
    wylacz_wszystko();
    wlacz('.koniec_quizu');
    var el = document.querySelector(".info_o_wyniku");
    el.textContent = "Twój wynik to " + (daj_wynik().toString()) + " sekund!";
}
function daj_wynik() {
    var starty = JSON.parse(window.localStorage.getItem('czasy_start'));
    var konce = JSON.parse(window.localStorage.getItem('czasy_koniec'));
    var poprawne = daj_aktualny_quiz().odpowiedzi;
    var kary = daj_aktualny_quiz().kary;
    var udzielone = JSON.parse(window.localStorage.getItem('moje_odp'));
    var ret = 0;
    var dlu = starty.length;
    for (var i = 0; i < dlu; i++) {
        console.log();
        if (udzielone[i] == poprawne[i])
            ret += parseInt(konce[i]) - parseInt(starty[i]);
        else //celowo jeśli jest źle to nie dodaję do wyniku spędzonego czasu, to chyba sensowniejsze
            ret += parseInt(kary[i]) * 1000;
    }
    return Math.ceil(ret / 1000);
}
function dodaj_do_wynikow() {
    var el = document.getElementById("nick");
    if (el.value == "")
        return;
    dodaj_wynik(el.value, daj_wynik());
    idz_do_wyniki();
}
