function czas(): number
{
    return (new Date()).getTime()
}

function touch(str : string)
{
    let x=JSON.parse(window.localStorage.getItem(str));
    if (x==null)
        x=[];
    window.localStorage.setItem(str, JSON.stringify(x));
}

function tryb(): string
{
    return window.localStorage.getItem('tryb');
}

function dodaj_wynik(nick: string, wynik: number)
{
    touch(tryb());
    let x=JSON.parse(window.localStorage.getItem(tryb()));
    x.push({wynik, nick});
    window.localStorage.setItem(tryb(), JSON.stringify(x));
}

function aktualizuj_wyniki()
{
    touch(tryb());
    let el: HTMLElement=document.querySelector('.miejsce_na_wyniki');
    while (el.children.length>1)
        el.removeChild(el.lastElementChild);
    let list=JSON.parse(window.localStorage.getItem(tryb()));
    list.sort
    (
        function(a, b)
        {
            if (a.wynik<b.wynik)
                return -1;
            if (a.wynik>b.wynik)
                return 1;
            return 0;
        }
    );
    while(list.length>10)
        list.pop();
    let id: number=0;
    for (let i in list)
    {
        id++;
        let wstaw=document.createElement('tr');
        wstaw.innerHTML='<td>'+id+'</td>'+'<td>'+list[i].nick+'</td>'+'<td>'+list[i].wynik+'</td>';
        el.appendChild(wstaw);
    }
}

function wylacz(str: string)
{
    let el: HTMLElement=document.querySelector(str);
    el.style.display='none';
}

function wlacz(str: string)
{
    let el: HTMLElement=document.querySelector(str);
    el.style.display='block';
}

function wylacz_wszystko()
{
    wylacz('.menu');
    wylacz('.wyniki');
    wylacz('.quiz');
    wylacz('.koniec_quizu');
}

function idz_do_menu()
{
    wylacz_wszystko();
    wlacz('.menu');
}

function idz_do_wyniki()
{
    wylacz_wszystko();
    aktualizuj_wyniki();
    wlacz('.wyniki');
}

function idz_do_quiz()
{
    wylacz_wszystko();
    wlacz('.quiz');
}

function reset_czasow()
{
    let list=[];
    let dlu: number=daj_aktualny_quiz().pytania.length;
    for (let i=1; i<=dlu; i++)
        list.push(0);
    window.localStorage.setItem('czasy_start', JSON.stringify(list));
    window.localStorage.setItem('czasy_koniec', JSON.stringify(list));
    window.localStorage.setItem('moje_odp', JSON.stringify(list));
}

function zacznij_quiz()
{
    idz_do_quiz();
    ustal_quiz();
    reset_czasow();
    idz_do_pytania(1);
}

let quiz_easy: string=
`{
    "pytania":
    [
        "7*8",
        "46+85",
        "39+87",
        "2+2*2"
    ],
    "odpowiedzi":
    [
        "56",
        "131",
        "126",
        "6"
    ],
    "kary":
    [
        "8",
        "15",
        "15",
        "5"
    ]
}`;

let quiz_hard: string=
`{
    "pytania":
    [
        "7821+1399",
        "8433+9232",
        "54*76",
        "12*115+34"
    ],
    "odpowiedzi":
    [
        "9220",
        "17665",
        "4104",
        "1414"
    ],
    "kary":
    [
        "20",
        "20",
        "45",
        "60"
    ]
}`;

function random_int(min, max)
{
    return Math.floor(min+Math.random()*(max-min+1));
}

function quiz_rand(): string
{
    let pyt: string='';
    let odp: string='';
    let kar: string='';
    const dlu=10;
    for (let i=0; i<dlu; i++)
    {
        if (i>0)
        {
            pyt+=',';
            odp+=',';
            kar+=',';
        }
        if (random_int(0, 1)==0)
        {
            let a: number=random_int(0, 100);
            let b: number=random_int(0, 100);
            pyt+='"'+a.toString()+'*'+b.toString()+'"';
            odp+='"'+(a*b).toString()+'"';
            kar+='"45"';
        }
        else
        {
            let a: number=random_int(0, 10000);
            let b: number=random_int(0, 10000);
            pyt+='"'+a.toString()+'+'+b.toString()+'"';
            odp+='"'+(a+b).toString()+'"';
            kar+='"20"';
        }
    }
    return '{"pytania":['+pyt+'],"odpowiedzi":['+odp+'],"kary":['+kar+']}';
}

function wybierz(str: string)
{
    let el1: HTMLElement=document.querySelector('.wybor_easy');
    let el2: HTMLElement=document.querySelector('.wybor_hard');
    let el3: HTMLElement=document.querySelector('.wybor_rand');
    el1.style.backgroundColor='';
    el2.style.backgroundColor='';
    el3.style.backgroundColor='';
    el1.style.border='';
    el2.style.border='';
    el3.style.border='';
    let el: HTMLElement=document.querySelector(str);
    el.style.backgroundColor='rgb(10, 126, 243)';
    el.style.border='3px solid rgb(51, 153, 255)';
}

function wybierz_easy()
{
    wybierz('.wybor_easy');
    idz_do_menu();
    window.localStorage.setItem('tryb', 'easy');
}

function wybierz_hard()
{
    wybierz('.wybor_hard');
    idz_do_menu();
    window.localStorage.setItem('tryb', 'hard');
}

function wybierz_rand()
{
    wybierz('.wybor_rand');
    idz_do_menu();
    window.localStorage.setItem('tryb', 'rand');
}

wybierz_easy();

function ustal_quiz()
{
    window.localStorage.setItem('quiz', daj_jakis_quiz())
}

function daj_jakis_quiz(): string
{
    if (tryb()=="easy")
        return quiz_easy;
    if (tryb()=="hard")
        return quiz_hard;
    if (tryb()=="rand")
        return quiz_rand();
}

function daj_aktualny_quiz()
{
    return JSON.parse(window.localStorage.getItem('quiz'));
}

function daj_aktualne_pytanie()
{
    return parseInt(window.localStorage.getItem('numer_pytania'));
}

function idz_do_pytania(x: number)
{
    czysc_odp();
    let el: HTMLElement=document.querySelector(".quiz header");
    el.textContent="PYTANIE "+(x.toString());
    let quiz=daj_aktualny_quiz();
    el=document.querySelector(".info_o_pytaniu");
    el.textContent=quiz.pytania[x-1]+"=?   ( "+quiz.kary[x-1]+"s kary )";
    window.localStorage.setItem('numer_pytania', x.toString());

    el=document.querySelector('.skoki_do_pytan');
    while (el.children.length>0)
        el.removeChild(el.lastElementChild);
    let dlu: number=daj_aktualny_quiz().pytania.length;

    let wez2=JSON.parse(window.localStorage.getItem('czasy_koniec'));

    for (let i=0; i<=dlu+1; i++)
    {
        if ((i==0 && x==1) || (i==dlu+1 && x==dlu))
            continue;
        let wstaw=document.createElement('button');
        let kt: number=i;
        if (i==0)
            kt=x-1;
        if (i==dlu+1)
            kt=x+1;
        wstaw.className="wybor klikalne";
        wstaw.onclick=function(){idz_do_pytania(kt)};
        wstaw.innerText=i.toString();
        if (i==0)
          wstaw.innerText="POPRZEDNIE";
        if (i==dlu+1)
            wstaw.innerText="NASTEPNE";
        if (wez2[kt-1]>0)
        {
            wstaw.style.backgroundColor='rgb(10, 126, 243)';
            wstaw.style.border='3px solid rgb(51, 153, 255)';
        }
        el.appendChild(wstaw);
    }

    if (wszystko_mam())
    {
        let wstaw=document.createElement('button');
        wstaw.className="wybor klikalne";
        wstaw.onclick=function(){zakoncz_quiz()};
        wstaw.innerText="ZAKOŃCZ QUIZ";
        el.appendChild(wstaw);
    }

    let wez1=JSON.parse(window.localStorage.getItem('czasy_start'));
    if (wez1[x-1]==0)
        wez1[x-1]=czas();
    window.localStorage.setItem('czasy_start', JSON.stringify(wez1));

    let wez3=JSON.parse(window.localStorage.getItem('moje_odp'));
    if (wez2[x-1]==0)
    {
        el=document.querySelector(".miejsce_na_zapisana");
        el.textContent="";
    }
    else
    {
        el=document.querySelector(".miejsce_na_zapisana");
        el.textContent="Twoja odpowiedź to: "+wez3[x-1].toString();
    }
}

function wszystko_mam(): boolean
{
    let wez=JSON.parse(window.localStorage.getItem('czasy_koniec'));
    let dlu: number=wez.length;
    for (let i=0; i<dlu; i++)
        if (wez[i]==0)
            return false;
    return true;
}

function czysc_odp()
{
    let el=document.getElementById("odpowiedz") as HTMLInputElement;
    el.value="";
}

function zatwierdz_odpowiedz()
{
    let el=document.getElementById("odpowiedz") as HTMLInputElement;
    if (el.value=="")
        return;
    let wez1=JSON.parse(window.localStorage.getItem('czasy_koniec'));
    wez1[daj_aktualne_pytanie()-1]=czas();
    window.localStorage.setItem('czasy_koniec', JSON.stringify(wez1));
    let wez2=JSON.parse(window.localStorage.getItem('moje_odp'));
    wez2[daj_aktualne_pytanie()-1]=parseInt(el.value);
    window.localStorage.setItem('moje_odp', JSON.stringify(wez2));
    czysc_odp();
    idz_do_pytania(daj_aktualne_pytanie());
}

function czysc_nick()
{
    let el=document.getElementById("nick") as HTMLInputElement;
    el.value="";
}

function zakoncz_quiz()
{
    czysc_nick();
    wylacz_wszystko();
    wlacz('.koniec_quizu');
    let el: HTMLElement=document.querySelector(".info_o_wyniku");
    el.textContent="Twój wynik to "+(daj_wynik().toString())+" sekund!";
}

function daj_wynik(): number
{
    let starty=JSON.parse(window.localStorage.getItem('czasy_start'));
    let konce=JSON.parse(window.localStorage.getItem('czasy_koniec'));
    let poprawne=daj_aktualny_quiz().odpowiedzi;
    let kary=daj_aktualny_quiz().kary;
    let udzielone=JSON.parse(window.localStorage.getItem('moje_odp'));
    let ret: number=0;
    let dlu: number=starty.length;
    for (let i=0; i<dlu; i++)
    {
        console.log();
        if (udzielone[i]==poprawne[i])
            ret+=parseInt(konce[i])-parseInt(starty[i]);
        else//celowo jeśli jest źle to nie dodaję do wyniku spędzonego czasu, to chyba sensowniejsze
            ret+=parseInt(kary[i])*1000;
    }
    return Math.ceil(ret/1000);
}

function dodaj_do_wynikow()
{
    let el=document.getElementById("nick") as HTMLInputElement;
    if (el.value=="")
        return;
    dodaj_wynik(el.value, daj_wynik());
    idz_do_wyniki();
}