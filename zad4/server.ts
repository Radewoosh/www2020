import * as sqlite3 from 'sqlite3';

const express=require('express');
const app=express();
const port: number=8080;
const my_title: string="Meme Market";

function get_time(): number
{
    return new Date().getTime()
}

function get_hour(x: number): string
{
    const date=new Date(x);
    const hours=date.getHours();
    const minutes="0"+date.getMinutes();
    const seconds="0"+date.getSeconds();
    const formatted_time=hours+':'+minutes.substr(-2)+':'+seconds.substr(-2);
    return formatted_time;
}

function get_value(meme_id: number, to_ret)
{
    const db=new sqlite3.Database('base.db');
    db.all('SELECT value FROM history WHERE id = ? ORDER BY time DESC;', [meme_id], function(err, rows)
    {
        if (err)
        {
            to_ret(-1);
            return;
        }
        to_ret(rows[0].value);
    });
}

function get_history_of_values(meme_id: number, to_ret)
{
    const db=new sqlite3.Database('base.db');
    db.all('SELECT value, time FROM history WHERE id = ? ORDER BY time DESC;', [meme_id], function(err, rows)
    {
        if (err)
        {
            to_ret([]);
            return;
        }
        const ret=[];
        let i: number=0;
        const n: number=rows.length;
        while (i<n)
        {
            let a: string;
            let b: string;
            if (i===0)
                a="Now";
            else
                a=get_hour(rows[i-1].time);
            b=get_hour(rows[i].time);
            ret.push({value: rows[i].value, from: b, to: a});
            i++;
        }
        to_ret(ret);
    });
}

function get_all_memes(to_ret)
{
    const db=new sqlite3.Database('base.db');
    db.all('SELECT id, path FROM memes ORDER BY id ASC;', [], function(err, rows)
    {
        if (err)
        {
            to_ret([]);
            return;
        }
        const ret=[];
        for(const {id, path} of rows)
            ret.push({id, path});
        db.close();
        to_ret(ret);
    });
}

function get_memes_with_values(to_ret)
{
    get_all_memes(function(my_memes)
    {
        const ret=[];
        const size=my_memes.length;
        function calc_value(i: number)
        {
            if (i===size)
            {
                to_ret(ret);
                return;
            }
            get_value(my_memes[i].id, function(value)
            {
                const id: number=my_memes[i].id;
                const path: string=my_memes[i].path;
                ret.push({id, path, value});
                calc_value(i+1);
            });
        }
        calc_value(0);
    });
}

app.set('view engine', 'pug');
app.use(express.urlencoded({extended: true}));
app.get('/', function(req, res)
{
    get_memes_with_values(function(my_memes)
    {
        my_memes.sort(function (a, b)
        {
            if (a.value>b.value)
                return -1;
            if (a.value<b.value)
                return 1;
            return 0;
        });
        while(my_memes.length>3)
            my_memes.pop();
        res.render('index', { title: my_title, memes: my_memes});
    });
});
app.get('/all', function(req, res)
{
    get_memes_with_values(function(my_memes)
    {
        res.render('all', { title: my_title, memes: my_memes});
    });
});
app.get('/meme/:meme_id', function(req, res)
{
    get_memes_with_values(function(my_memes)
    {
        for (const i of my_memes)
        {
            if (i.id.toString()===req.params.meme_id)
            {
                get_history_of_values(i.id, function(my_history)
                {
                    res.render('meme', { title: my_title, meme: i, history: my_history});
                });
                return;
            }
        }
        res.render('error', { title: my_title});
    });
});
app.post('/meme/:meme_id', function (req, res)
{
    const db=new sqlite3.Database('base.db');
    db.run('INSERT INTO history (id, value, time) VALUES (?, ?, ?);', [parseInt(req.params.meme_id, 10), parseInt(req.body.new_value, 10), get_time()]);
    get_memes_with_values(function(my_memes)
    {
        for (const i of my_memes)
        {
            if (i.id.toString()===req.params.meme_id)
            {
                get_history_of_values(i.id, function(my_history)
                {
                    res.render('meme', { title: my_title, meme: i, history: my_history});
                });
                return;
            }
        }
        res.render('error', { title: my_title});
    });
});
app.all('*', function (req, res)
{
    res.render('error', { title: my_title});
});

app.listen(port);