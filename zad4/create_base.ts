import * as sqlite3 from 'sqlite3';

function get_time(): number
{
    return new Date().getTime()
}

function random_int(min: number, max: number): number
{
    return Math.floor(min+Math.random()*(max-min+1));
}

function create_base()
{
    sqlite3.verbose();
    const db=new sqlite3.Database('base.db');
    db.exec('DROP TABLE IF EXISTS memes;');
    db.exec('CREATE TABLE memes (id INT, path VARCHAR(1023));');
    db.exec('DROP TABLE IF EXISTS history;');
    db.exec('CREATE TABLE history (id INT, value INT, time INT);');

    const lineByLine=require('n-readlines');
    const liner=new lineByLine('links.txt');
    let line: { toString: () => string; };
    while (line=liner.next())
    {
        const x: number=random_int(0, 1000000000);
        db.run('INSERT INTO memes (id, path) VALUES (?, ?);', [x, line.toString()]);
        db.run('INSERT INTO history (id, value, time) VALUES (?, ?, ?);', [x, random_int(0, 100), get_time()]);
    }

    db.close();
}

create_base();