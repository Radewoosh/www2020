import { fibo } from "./fibo";
import { expect } from "chai";
import "mocha";
import {Builder, Capabilities} from 'selenium-webdriver';
import { driver } from 'mocha-webdriver';
import {assert} from "chai";

describe("Fibonacci", () =>
{

    it("small values", () =>
    {
        expect(fibo(0)).to.equal(0);
        expect(fibo(1)).to.equal(1);
        expect(fibo(2)).to.equal(1);
        expect(fibo(3)).to.equal(2);
        expect(fibo(4)).to.equal(3);
        expect(fibo(5)).to.equal(5);
    });
    it("a few big", () =>
    {
        expect(fibo(30)).to.equal(832040);
        expect(fibo(40)).to.equal(102334155);
        expect(fibo(25)).to.equal(75025);
    });

});

function delay(ms: number)
{
    return new Promise( resolve => setTimeout(resolve, ms) );
}

function dzis(): string
{
    var today=new Date();
    var dd=String(today.getDate()).padStart(2, '0');
    var mm=String(today.getMonth() + 1).padStart(2, '0');
    var yyyy=today.getFullYear();

    return yyyy+'-'+mm+'-'+dd;
}

async function nieklik(str: string)
{
    expect(await driver.find(str).click()
    .then(() =>
    {
        return false;//klikalne (i kliknalem)
    })
    .catch(() =>
    {
        return true;//nieklikalne
    })
    ).to.equal(true);
}

describe("Zaslona", () =>
{
    for (let i=0; i<2; i++)
    {
        let sciezka=``;
        if (i===0)
            sciezka=`file://${process.cwd()}/page.html`;
        else
            sciezka=`file://${process.cwd()}/subpage.html`;
        it('miasta_startowe', async function()
        {
            this.timeout(200000);
            await driver.get(sciezka);
            expect(await driver.find('#skad').getText()).to.include('Warszawa');
            expect(await driver.find('#skad').getText()).to.include('Radom');
            expect(await driver.find('#skad').getText()).to.include('Konstantynopol');
            expect(await driver.find('#skad').getText()).to.include('Szczecin');
            expect(await driver.find('#skad').getText()).to.not.include('Białystok');
            expect(await driver.find('#skad').getText()).to.not.include('San Francisco');
            expect(await driver.find('#skad').getText()).to.not.include('Moskwa');
            expect(await driver.find('#skad').getText()).to.not.include('Rio de Janeiro');
        });
        it('miasta_koncowe', async function()
        {
            this.timeout(200000);
            await driver.get(sciezka);
            expect(await driver.find('#dokad').getText()).to.include('Białystok');
            expect(await driver.find('#dokad').getText()).to.include('San Francisco');
            expect(await driver.find('#dokad').getText()).to.include('Moskwa');
            expect(await driver.find('#dokad').getText()).to.include('Rio de Janeiro');
            expect(await driver.find('#dokad').getText()).to.not.include('Warszawa');
            expect(await driver.find('#dokad').getText()).to.not.include('Radom');
            expect(await driver.find('#dokad').getText()).to.not.include('Konstantynopol');
            expect(await driver.find('#dokad').getText()).to.not.include('Szczecin');
        });
        it('pan_prezydent', async function()
        {
            this.timeout(200000);
            await driver.get(sciezka);
            if (i===1)
                driver.executeScript("window.scrollBy(0,1000)");
            await driver.find('#kiedy').sendKeys(dzis());
            nieklik('.zaslona');
            await driver.find('#wyslij').doClick();
            nieklik('#wyslij');
            nieklik('#czysc');
            expect(await driver.find('.zaslona').isDisplayed()).to.equal(true);
            await driver.find('.zaslona').doClick();
            nieklik('.zaslona');
            expect(await driver.find('.zaslona').isDisplayed()).to.equal(false);
        });
        it('wszystko_dobrze', async function()
        {
            this.timeout(200000);
            await driver.get(sciezka);
            if (i===1)
                driver.executeScript("window.scrollBy(0,1000)");
            await driver.find('#czysc').doClick();
            await driver.find('#kiedy').sendKeys(dzis());
            await driver.find('#imie').sendKeys("Mateusz");
            await driver.find('#nazwisko').sendKeys("Radecki");
            await driver.find('#skad').sendKeys("Warszawa");
            await driver.find('#dokad').sendKeys("Białystok");
            nieklik('.zaslona');
            await driver.find('#wyslij').doClick();
            nieklik('#wyslij');
            nieklik('#czysc');
            expect(await driver.find('.zaslona').isDisplayed()).to.equal(true);
        });
        it('brak_imienia', async function()
        {
            this.timeout(200000);
            await driver.get(sciezka);
            if (i===1)
                driver.executeScript("window.scrollBy(0,1000)");
            await driver.find('#czysc').doClick();
            await driver.find('#kiedy').sendKeys(dzis());
            await driver.find('#nazwisko').sendKeys("Radecki");
            await driver.find('#skad').sendKeys("Warszawa");
            await driver.find('#dokad').sendKeys("Białystok");
            nieklik('.zaslona');
            await driver.find('#wyslij').doClick();
            nieklik('.zaslona');
            expect(await driver.find('.zaslona').isDisplayed()).to.equal(false);
        });
        it('stara_data', async function()
        {
            this.timeout(200000);
            await driver.get(sciezka);
            if (i===1)
                driver.executeScript("window.scrollBy(0,1000)");
            await driver.find('#czysc').doClick();
            await driver.find('#kiedy').sendKeys('2020-01-01');
            await driver.find('#imie').sendKeys("Mateusz");
            await driver.find('#nazwisko').sendKeys("Radecki");
            await driver.find('#skad').sendKeys("Warszawa");
            await driver.find('#dokad').sendKeys("Białystok");
            nieklik('.zaslona');
            await driver.find('#wyslij').doClick();
            nieklik('.zaslona');
            expect(await driver.find('.zaslona').isDisplayed()).to.equal(false);
        });
    }
});