const kolory: string[] = ["red", "orange", "yellow", "green", "blue", "indigo", "purple"];

function disco(mod : number)
{
    setTimeout(() =>
    {
        document.body.style.backgroundColor = kolory[mod%7];
        disco((mod+1)%7);
    }, 300);
}

function start_disco(opoznienie: number)
{
    return new Promise(function(resolve, reject)
    {
        setTimeout(() =>
        {
            disco(0);
        }, opoznienie);
    });
}

if (false)//tryb disco
{
    start_disco(5000);
}

{//szukam najwyższego identyfikatora pasażera
    let pasa=document.querySelector(".pasazerowie");
    if (pasa!=null)
    {
        let list=pasa.children;
        let len=list.length;
        let g=-1;
        for (let i=0; i<len; i++)
        {
            let on=list[i];
            if (g==-1 || list[i].getAttribute("data-identyfikator-pasazera")>list[g].getAttribute("data-identyfikator-pasazera"))
            {
                g=i;
            }
        }
        console.log(list[g].childNodes[0].textContent);
    }
}

fetch('https://api.github.com/repos/Microsoft/TypeScript/commits')
.then((response) =>
{
    return response.json();
})
.then((data) =>
{
    let x=data[0];
    if (x==undefined)
        return;
    x=x["author"];
    if (x==undefined)
        return;
    x=x["avatar_url"];
    if (x==undefined)
        return;
    let y: HTMLImageElement=document.querySelector('img[alt="logo"]');
    y.src=x;
});

function losowy_kolor()
{
    return kolory[Math.floor(7*Math.random())];
}

function fibo2(x : number)
{
    if (x==0)
    {
        return [0, 1];
    }
    if (x%2==1)
    {
        let wez=fibo2(x-1);
        return [wez[1], wez[0]+wez[1]];
    }
    let wez=fibo2(x/2);
    return [wez[0]*(2*wez[1]-wez[0]), wez[0]*wez[0]+wez[1]*wez[1]];
}

function fibo(x : number)
{
    return fibo2(x)[0];
}

let licz_kliki=0;

function zmien_kolor(event)
{
    licz_kliki++;
    console.log(fibo(licz_kliki*10));
    let zakaz=document.querySelector(".formularz");
    if (zakaz.contains(event.target) == false)
        event.target.style.backgroundColor=losowy_kolor();
}

function wyczysc()
{
    {
        let el=document.getElementById("skad") as HTMLSelectElement;
        el.selectedIndex=0;
    }
    {
        let el=document.getElementById("dokad") as HTMLSelectElement;
        el.selectedIndex=0;
    }
    {
        let el=document.getElementById("imie") as HTMLInputElement;
        el.value="";
    }
    {
        let el=document.getElementById("nazwisko") as HTMLInputElement;
        el.value="";
    }
    {
        let el=document.getElementById("kiedy") as HTMLInputElement;
        el.value="";
    }
    return false;
}

function dzis()
{
    var today=new Date();
    var dd=String(today.getDate()).padStart(2, '0');
    var mm=String(today.getMonth() + 1).padStart(2, '0');
    var yyyy=today.getFullYear();

    return yyyy+'-'+mm+'-'+dd;
}

function wywyslij()
{
    {
        let el=document.getElementById("kiedy") as HTMLInputElement;
        if ((dzis()<=el.value)==false)
        {
            return true;
        }
    }
    {
        let el=document.getElementById("imie") as HTMLInputElement;
        if (el.value=="")
        {
            return true;
        }
    }
    {
        let el=document.getElementById("nazwisko") as HTMLInputElement;
        if (el.value=="")
        {
            return true;
        }
    }
    //poprawnie wypełniony
    {
        let el=document.querySelector(".dane1") as HTMLElement;
        let im=document.getElementById("imie") as HTMLInputElement;
        let na=document.getElementById("nazwisko") as HTMLInputElement;
        el.textContent=im.value+" "+na.value;
    }
    {
        let el=document.querySelector(".dane2") as HTMLElement;
        let sk=document.getElementById("skad") as HTMLSelectElement;
        let dok=document.getElementById("dokad") as HTMLSelectElement;
        el.textContent=sk.value+" - "+dok.value;
    }
    {
        let el=document.querySelector(".dane3") as HTMLElement;
        let ki=document.getElementById("kiedy") as HTMLInputElement;
        el.textContent=ki.value;
    }
    zaslon();
    wyczysc();
    return true;
}

function zaslon()
{
    let el=document.querySelector(".zaslona") as HTMLElement;
    el.style.display="inline";
}

function odslon()
{
    let el=document.querySelector(".zaslona") as HTMLElement;
    el.style.display="none";
}

window.addEventListener('DOMContentLoaded', (event) =>
{
    {
        let el=document.getElementById("imie") as HTMLInputElement;
        el.value="Barack";
    }
    {
        let el=document.getElementById("nazwisko") as HTMLInputElement;
        el.value="Obama";
    }
});