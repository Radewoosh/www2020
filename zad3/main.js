var kolory = ["red", "orange", "yellow", "green", "blue", "indigo", "purple"];
function disco(mod) {
    setTimeout(function () {
        document.body.style.backgroundColor = kolory[mod % 7];
        disco((mod + 1) % 7);
    }, 300);
}
function start_disco(opoznienie) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            disco(0);
        }, opoznienie);
    });
}
if (false) //tryb disco
 {
    start_disco(5000);
}
{ //szukam najwyższego identyfikatora pasażera
    var pasa = document.querySelector(".pasazerowie");
    if (pasa != null) {
        var list = pasa.children;
        var len = list.length;
        var g = -1;
        for (var i = 0; i < len; i++) {
            var on = list[i];
            if (g == -1 || list[i].getAttribute("data-identyfikator-pasazera") > list[g].getAttribute("data-identyfikator-pasazera")) {
                g = i;
            }
        }
        console.log(list[g].childNodes[0].textContent);
    }
}
fetch('https://api.github.com/repos/Microsoft/TypeScript/commits')
    .then(function (response) {
    return response.json();
})
    .then(function (data) {
    var x = data[0];
    if (x == undefined)
        return;
    x = x["author"];
    if (x == undefined)
        return;
    x = x["avatar_url"];
    if (x == undefined)
        return;
    var y = document.querySelector('img[alt="logo"]');
    y.src = x;
});
function losowy_kolor() {
    return kolory[Math.floor(7 * Math.random())];
}
function fibo2(x) {
    if (x == 0) {
        return [0, 1];
    }
    if (x % 2 == 1) {
        var wez_1 = fibo2(x - 1);
        return [wez_1[1], wez_1[0] + wez_1[1]];
    }
    var wez = fibo2(x / 2);
    return [wez[0] * (2 * wez[1] - wez[0]), wez[0] * wez[0] + wez[1] * wez[1]];
}
function fibo(x) {
    return fibo2(x)[0];
}
var licz_kliki = 0;
function zmien_kolor(event) {
    licz_kliki++;
    console.log(fibo(licz_kliki * 10));
    var zakaz = document.querySelector(".formularz");
    if (zakaz.contains(event.target) == false)
        event.target.style.backgroundColor = losowy_kolor();
}
function wyczysc() {
    {
        var el = document.getElementById("skad");
        el.selectedIndex = 0;
    }
    {
        var el = document.getElementById("dokad");
        el.selectedIndex = 0;
    }
    {
        var el = document.getElementById("imie");
        el.value = "";
    }
    {
        var el = document.getElementById("nazwisko");
        el.value = "";
    }
    {
        var el = document.getElementById("kiedy");
        el.value = "";
    }
    return false;
}
function dzis() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    return yyyy + '-' + mm + '-' + dd;
}
function wywyslij() {
    {
        var el = document.getElementById("kiedy");
        if ((dzis() <= el.value) == false) {
            return true;
        }
    }
    {
        var el = document.getElementById("imie");
        if (el.value == "") {
            return true;
        }
    }
    {
        var el = document.getElementById("nazwisko");
        if (el.value == "") {
            return true;
        }
    }
    //poprawnie wypełniony
    {
        var el = document.querySelector(".dane1");
        var im = document.getElementById("imie");
        var na = document.getElementById("nazwisko");
        el.textContent = im.value + " " + na.value;
    }
    {
        var el = document.querySelector(".dane2");
        var sk = document.getElementById("skad");
        var dok = document.getElementById("dokad");
        el.textContent = sk.value + " - " + dok.value;
    }
    {
        var el = document.querySelector(".dane3");
        var ki = document.getElementById("kiedy");
        el.textContent = ki.value;
    }
    zaslon();
    wyczysc();
    return true;
}
function zaslon() {
    var el = document.querySelector(".zaslona");
    el.style.display = "inline";
}
function odslon() {
    var el = document.querySelector(".zaslona");
    el.style.display = "none";
}
window.addEventListener('DOMContentLoaded', function (event) {
    {
        var el = document.getElementById("imie");
        el.value = "Barack";
    }
    {
        var el = document.getElementById("nazwisko");
        el.value = "Obama";
    }
});
