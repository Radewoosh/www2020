export function fibo(x : number)
{
    if (x==0)
        return 0;
    if (x<=2)
        return 1;
    return fibo(x-1)+fibo(x-2);
}