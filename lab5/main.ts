function zaloguj(...komunikaty: string[]) {

    console.log("Ależ skomplikowany program!", ...komunikaty);

}


zaloguj("Ja", "cię", "nie", "mogę");

let jsonString: string = `{

    "piloci": [

        "Pirx",

        "Exupery",

        "Idzikowski",

        "Główczewski"
    ],

    "lotniska": {

        "WAW": ["Warszawa", [3690, 2800]],

        "NRT": ["Narita", [4000, 2500]],

        "BQH": ["Biggin Hill", [1802, 792]],

        "LBG": ["Paris-Le Bourget", [2665, 3000, 1845]]
    }

}`;

interface ILotnisko
{
    [id: string]: [string, number[]];
}

type Pilot =
{
    nazwisko: string;
}

interface ILiniaLotnicza
{
    piloci: Pilot[];
    lotniska: ILotnisko;
}

let daneLiniiLotniczej: ILiniaLotnicza = JSON.parse(jsonString);

function sprawdzDaneLiniiLotniczej(dane: any): dane is ILiniaLotnicza
{
    return czyMaPola(dane, ["piloci", "lotniska"])
        && czyToPiloci(dane.piloci)
        && czyToLotniska(dane.lotniska);
}

function czyMaPola(dane: any, pola: string[]) : boolean
{
    for (const i of pola)
    {
        if (i in dane === false)
        {
            return false;
        }
    }
    return true;
}


function czyToPiloci(dane: any) : boolean
{
    return czyToTablicaTypu(dane, "string");
}

function czyToLotniska(dane: any) : boolean
{
    for (const i in dane)
    {
        if (czyToLotnisko(dane[i]) === false)
        {
            return false;
        }
    }
    return true;
}

function czyToLotnisko(dane : any) : boolean
{
    return czyToTablica(dane)
        && dane.length === 2
        && czyToTypu(dane[0], "string")
        && czyToTablicaTypu(dane[1], "number");
}

function czyToTablicaTypu(dane: any, typ: string): boolean
{
    return czyToTablica(dane) && czyWszystkieTypu(dane, typ);
}

function czyWszystkieTypu(dane: any, typ: string) : boolean
{
    for (const i of dane)
    {
        if (czyToTypu(i, typ) === false)
        {
            return false;
        }
    }
    return true;
}

function czyToTypu(dane: any, typ: string): boolean
{
    return typeof dane === typ;
}

function czyToTablica(dane: any): boolean
{
    return dane instanceof Array;
}