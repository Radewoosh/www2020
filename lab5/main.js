var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
function zaloguj() {
    var komunikaty = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        komunikaty[_i] = arguments[_i];
    }
    console.log.apply(console, __spreadArrays(["Ależ skomplikowany program!"], komunikaty));
}
zaloguj("Ja", "cię", "nie", "mogę");
var jsonString = "{\n\n    \"piloci\": [\n\n        \"Pirx\",\n\n        \"Exupery\",\n\n        \"Idzikowski\",\n\n        \"G\u0142\u00F3wczewski\"\n    ],\n\n    \"lotniska\": {\n\n        \"WAW\": [\"Warszawa\", [3690, 2800]],\n\n        \"NRT\": [\"Narita\", [4000, 2500]],\n\n        \"BQH\": [\"Biggin Hill\", [1802, 792]],\n\n        \"LBG\": [\"Paris-Le Bourget\", [2665, 3000, 1845]]\n    }\n\n}";
var daneLiniiLotniczej = JSON.parse(jsonString);
function sprawdzDaneLiniiLotniczej(dane) {
    return czyMaPola(dane, ["piloci", "lotniska"])
        && czyToPiloci(dane.piloci)
        && czyToLotniska(dane.lotniska);
}
function czyMaPola(dane, pola) {
    for (var _i = 0, pola_1 = pola; _i < pola_1.length; _i++) {
        var i = pola_1[_i];
        if (i in dane === false) {
            return false;
        }
    }
    return true;
}
function czyToPiloci(dane) {
    return czyToTablicaTypu(dane, "string");
}
function czyToLotniska(dane) {
    for (var i in dane) {
        if (czyToLotnisko(dane[i]) === false) {
            return false;
        }
    }
    return true;
}
function czyToLotnisko(dane) {
    return czyToTablica(dane)
        && dane.length === 2
        && czyToTypu(dane[0], "string")
        && czyToTablicaTypu(dane[1], "number");
}
function czyToTablicaTypu(dane, typ) {
    return czyToTablica(dane) && czyWszystkieTypu(dane, typ);
}
function czyWszystkieTypu(dane, typ) {
    for (var _i = 0, dane_1 = dane; _i < dane_1.length; _i++) {
        var i = dane_1[_i];
        if (czyToTypu(i, typ) === false) {
            return false;
        }
    }
    return true;
}
function czyToTypu(dane, typ) {
    return typeof dane === typ;
}
function czyToTablica(dane) {
    return dane instanceof Array;
}
//# sourceMappingURL=main.js.map